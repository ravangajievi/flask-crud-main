## Setting Up Your Development Environment and Running the Application Locally
```
git clone https://github.com/ravangajievi/flask-crud.git
cd flask-crud
```

Install requirements
```
pip install -r requirements.txt
```
Build docker image
```
docker build -t my-flask-app .
```

Run image
```
docker run -p 80:5000 my-flask-app
```
access localhost from the browser

## Deploy Application
I deployed application on digital ocean

http://167.172.101.132/
